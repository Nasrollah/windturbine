## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-12

## -*- texinfo -*-
## @defun {@var{} =} air_density (@var{T}, @var{P}, @var{H})
## 
## T: Temperature in Celcius
## P: Presure in Pascals
## H: Relative humidity
##
## Reference: @url{https://en.wikipedia.org/wiki/Density_of_air}
## @seealso{}
## @end defun

function [rho Psat] = air_density (Tc, P, H)

  ## Constants
  R  = 8.314;     #[J/K/mol] Universal gas constant
  Rd = 287.058;   #[J/kg/K] Specific gas constant for dry air
  Rv = 461.495;   #[J/kg/K] Specific gas constant for water vapor

  # Conversion Pvsat --> Pv
  kPv = 6.1078e2;
  aPv = 7.5;
  bPv = 237.3;

  T = Tc + 273.15; # Kelvin

  Psat = kPv * 10 .^ (aPv * Tc ./ (Tc + bPv)); # Pa
  Pv   = H .* Psat;                            # Pa
  Pd   = P - Pv;                               # Pa
  
  rho = Pd ./ (Rd * T) + Pv ./ (Rv * T);

endfunction

%!test
%! T = 35:-5:-25; # C
%! P = 101.325e3; # Pa
%! H = 0;
%! rho = [1.1455, 1.1644, 1.1839, 1.2041, 1.2250, 1.2466, 1.2690, 1.2922, ...
%!        1.3163, 1.3413, 1.3673, 1.3943, 1.4224];
%! assert (air_density(T,P,H), rho, 1e-4);


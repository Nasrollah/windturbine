# Experimental data analysis
This folder contains m-files, implementing scripts and functions 
to analyze experimental results.


## File name convention
Files implementing function or function libraries should be named in 
lowercase and they should **not** start with `s_`.

Files implementing scripts should have their name appending with `s_`.

For example, a file named `myfunction.m` contains a main function called 
`myfunction`. 
A file named `s_myscript.m` contains a script hat might define its 
own functions.

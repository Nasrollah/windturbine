## Copyright (C) 2008-2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{idx} =} pickplot2d (@var{x}, @var{y})
## @defunx {@var{idx} =} pickplot2d (@var{h}, @dots{})
## 
## @seealso{ginput}
## @end defun

function idx = pickplot2d (fig, x, y, fmt)

  if !ishandle (fig)
    y = x;
    x = fig;
    fig = figure ();
  else
    figure (fig);
  endif
  plot (x, y, fmt);
  axis tight;

  basetxt = ['Click the %s button to define\nthe %s of the selection box.\n' ...
             'Click %s button to cancel'];
  %corner = {'upper-left','botton-right'};
  corner = {'1st corner','2nd corner'};
  button = {'left','middle','right'};

  % box
  xrange = [min(x), max(x)];
  yrange = [min(y), max(y)];

  hold on
  hp = plot (x, y,'sr');
  set(hp, 'visible', 'off')
  axis tight
  hold off

  htxt = text (0.05,0.9,'','Units','normalized');

  idx = [];
  done = 0;
  while done ~=1
    %% Upper-left
    txt = sprintf (basetxt, button{1}, corner{1}, button{3});
    set (htxt, 'String', txt);
    btn = 0;

    while btn ~= 1
     [pickx picky btn] = ginput (1);
     if btn == 3
        disp ('Aborting...')
        return
     endif
    endwhile
    xrange(1) = pickx;
    yrange(1) = picky;

    %% Bottom-right
    txt = sprintf (basetxt, corner{2}, button{1}, button{3});
    set (htxt, 'String', txt);
    btn = 0;

    while btn ~= 1
      [pickx picky btn] = ginput(1);
      if btn == 3
         disp ('Aborting...')
         return
      endif
    endwhile
    xrange(2) = pickx;
    yrange(2) = picky;

    xrange = sort (xrange, 'ascend');
    yrange = sort (yrange, 'ascend');
    
    indx = x >= xrange(1) & x <= xrange(2);
    indy = y >= yrange(1) & y <= yrange(2);
    idx  = sort (find (indx & indy));

    set (hp, 'xdata', x(idx), 'ydata', y(idx))
    set (hp, 'visible', 'on')
    txt = sprintf (['Happy with the selection?\nPress %s button to restart or'...
                    ' %s button to finish.'], button{2}, button{1});
    set (htxt, 'String', txt)

    [pickx picky btn] = ginput (1);
    if btn == 2
       done = 0;
       set (hp, 'visible', 'off')
    else
       done = 1;
       set (htxt, 'visible', 'off')
       set(hp, 'visible', 'off')
    endif

  endwhile

endfunction

## Copyright (C) 2017 - Simon Hasler
## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Hasler Simon <simon.hasler@hsr.ch>

## -*- texinfo -*-
## @defun {[@var{T} @var{W}] =} maxonRE50_model (@var{V}, @var{I})
## @defunx {[@dots{} @var{Tg} @var{Wg}] =} maxonRE50_model (@var{V}, @var{I}, @var{gear})
## Returns the torque @var{T} and rotational speed @var{W} of the DC-motor Maxon RE50 
## based on the aplied voltage @var{V} and current @var{I}.
##
## @var{V} should be in volts and @var{I} in amperes.
## @var{T} is given in Nm and @var{W} in rad/s.
##
## If a gearbox is attached to the motor, the reduction ratio  and its efficiency
## should be given in the structure @var{gear}, i.e. 
## @code{@var{gear} = struct ('ratio', @var{r}, 'efficiency', @var{f}})}; where
## @var{r} and @var{f} ar ethe ratio and efficiancy of the gearbox, resp.
## The reduced speed and torque are returned in @var{Tg} and @var{Wg}.
##
## Run @code{demo maxonRE50_model} for examples of use.
##
## Reference: @url{https://www.maxonmotor.com/maxon/view/product/370354}
## @end defun

function [T W varargout] = maxonRE50_model (V, I, gear = [])

  ## Datasheet values
  kM   = 242;   # Torque constant [mNm/A].
  kn   = 39.5;  # Rotational speed constant [rpm/V] 
  dndM = 0.638; # Speed-Torque gradient [rpm/mNm].

  fT = (1/1e3);               # conversion factor mNm --> Nm
  fW = (2*pi / 1) * (1 / 60); # conversion factor rpm --> rad/s

  # Results. 
  # See: https://www.maxonmotor.ch/medias/sys_master/8802918137886/maxon-Formelsammlung-de.pdf (German)
  #      http://storkdrives.com/wp-content/uploads/2013/10/1-Formelsamling.pdf (English)
  # Section 6.3
  T = kM * I;            # torque [mNm]
  W = kn * V - dndM * T; # speed [rpm]

  # Conversion
  T *= fT;
  W *= fW;

  if !isempty (gear)
    Tg = T; Wg = W;
    Wg /= gear.ratio;
    Tg *= gear.ratio * gear.efficiency;
    if nargout > 2; varargout{1} = Tg; endif
    if nargout > 3; varargout{2} = Wg; endif
  endif

endfunction

%!demo
%! P = 200; # Power in Watts
%! V = linspace (70, 300, 100).';
%! I = P ./ V;
%! [T W] = maxonRE50_model (V, I);
%!
%! figure (1);
%! plot (T * 1e3, W /2/pi * 60, '-k','linewidth', 2);
%! grid on
%! axis tight
%! xlabel ("Torque M [mNm]")
%! ylabel ("Ang. speed [rpm]");
%! title ("Torque-Speed curve @ 200W")

%!demo
%! P = 200; # Power in Watts
%! V = linspace (70, 300, 100).';
%! I = P ./ V;
%! gear_GP52 = struct ('ratio', 4.3, 'efficiency', 0.91);
%! [T W Tg Wg] = maxonRE50_model (V, I, gear_GP52);
%! figure (1);
%! plot (Tg * 1e3, Wg /2/pi * 60, '-k','linewidth', 2);
%! grid on
%! axis tight
%! xlabel ("Torque M [mNm]")
%! ylabel ("Ang. speed [rpm]");
%! title ("Torque-Speed curve @ 200W with 4.3:1 gear")


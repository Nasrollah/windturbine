## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-02-02

## Script to preprocess measured data
#
# add folder with data analysis functions

fname_raw = @(w) sprintf ("rawdata_vp%s.dat", strrep (num2str (w), '.', '_'));
fname     = @(w) sprintf ("vp%s.dat", strrep (num2str (w), '.', '_'));
vars      = {'gearGP52', 'radiusR', 'HR', 'dr', 'dH', ...
             'tauM', 'tauR', 'tauM_std', 'tauR_std', ...
             'omegaM', 'omegaR', 'omegaM_std', 'omegaR_std', ...
             'omegaR_data', 'omegaR_data_std', ...
             'vtR', 'vtR_std', 'vtR_data', 'vtR_data_std', 'v'};

txt_label = @(v) {sprintf('%.1f m/s down',v), sprintf('%.1f m/s up',v)};

vp = 6;
load (fname_raw(vp))

if ~exist ('vtR', 'var')
  # properties of gearbox installed in motor
  gearGP52 = struct ('ratio', 4.3, 'efficiency', 0.91);
  # Properties of the rotor
  radiusR  = 0.35; # in meters
  dr       = 0.01;
  HR       = 0.50;
  dH       = 0.01;
  # Error of the mean from interval
  # Measurements are consider 0.99 confidence intervals
  coeff = 1 ./ erfinv (0.99);
  lu2std = @(LU) squeeze (std (LU, 0, 2)) * coeff / sqrt (2);

  ##
  # Motor/rotor torque and rotation
  [TM, WM, TR, WR]         = maxonRE50_model (U, I, gearGP52);
  [TMlu, WMlu, TRlu, WRlu] = maxonRE50_model (U, I_lu, gearGP52);

  tauM = TM; tauM_std = lu2std (TMlu);
  tauR = TR; tauR_std = lu2std (TRlu);
  omegaM = WM; omegaM_std = lu2std (WMlu);
  omegaR = WR; omegaR_std = lu2std (WRlu);
  
  omegaR_data      = 2 * pi * fe;
  omegaR_data_std  = 2 * pi * fe_err;
  vtR_data         = omegaR_data * radiusR;
  vtR_data_std     = omegaR_data_std * radiusR .* ones(size(fe));

endif

## Plots
#

##
# Motor voltage and current at stead state
# The voltage was scanned with increasing (up) and decreasing values (down)
# for two values of the wind speed.
figure (1)
clf
h = plot (U,I,'o-');
axis tight
xlabel ('Voltage [V]')
ylabel ('Current [A]')
legend (h, txt_label(v));

##
# Motor rotational frequency as measured from the encoder and
# as caculated using the manufacturer's model.
# The plots show that the error propagated through the model 
# is an over estimation of the uncertainty in the angular speed.
# Therefore we assign the erro of th emeasured speed to the model output.

figure (2)
clf
fm  = omegaM / 2 / pi; 
if ~exist ('vtR', 'var')
  subplot (1,2,1)
  errf = omegaM_std / 2 / pi;
  h   = errorbar (fm, fe, errf, fe_err*ones(size(errf)),'~>o');
  ff  = [fm(:) fe(:)]; ff(!isfinite(ff(:,2)),:) = [];
  vax = [min(ff(:)) max(ff(:))];
  vax = vax([1 2 1 2]);
  axis (vax);
  line (vax(1:2), vax(3:4));
  xlabel ('Freq. motor (model) [Hz]')
  ylabel ('Freq. encoder (measured) [Hz]')
  legend (h, txt_label(v), ...
  'Location', 'NorthOutside','Orientation', 'horizontal');

  omegaM_std  = fe_err * 2 * pi;
  omegaR_std = omegaM_std / gearGP52.ratio;

  subplot (1,2,2)
endif

ferr = fe_err * ones(size(fm));
h = errorbar (fm, fe, ferr, ferr,'~>o');
if exist ('vtR', 'var')
  axis tight; 
  vax = axis (); 
else
  axis (vax);
endif
line (vax(1:2), vax(3:4));
xlabel ('Freq. motor (model) [Hz]')
ylabel ('Freq. encoder (measured) [Hz]')
legend (h, txt_label(v), ...
'Location', 'NorthOutside','Orientation', 'horizontal');

print ('-dpng', sprintf('freqR_vs_freqRmodel_%d.png',v))
##
# Torque of the motor applied to the turbine rotor vs. tangential speed of
# turbine rotor
# Tangential speed
vtR     = omegaR * radiusR;
vtR_std = omegaR_std * radiusR;

figure (3)
clf
errV = vtR_std * ones (size (vtR));
h    = errorbar (vtR, tauR, errV, tauR_std, '~>o');
axis tight
xlabel ('Tangetial speed rotor (model) [rad/s]')
ylabel ('\tau_M [Nm]')
legend (h, txt_label(v), ...
'Location', 'NorthOutside','Orientation', 'horizontal');


figure (4)
clf
h  = errorbar (vtR_data / v, tauR, vtR_data_std / v, tauR_std, '~>o');
axis tight
xlabel ('TSR [a.u.]')
ylabel ('\tau_M [Nm]')
legend (h, txt_label(v), ...
'Location', 'NorthOutside','Orientation', 'horizontal');

print ('-dpng', sprintf('tauR_vs_tsr_%d.png',v))

## Save data
#
fn = fname (vp);
save (fn, vars{:});
printf ("Saved to %s\n", fn); fflush (stdout);

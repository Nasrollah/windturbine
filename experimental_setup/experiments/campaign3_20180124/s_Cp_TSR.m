## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-02-02

## Script to preprocess measured data
#

## Load Friction
# Torque due to friction as indentified independently, see |s_friction.m| for
# the generation of the file used here
fname_F = '../campaign1_20180110/tauF.dat';
load (fname_F)

## Load pre-processed data
# The raw data, collected with |s_rawdata.m|, is pre-processed in the script
# |s_preprocess_rawdata.m|.
fname_vp  = @(w) sprintf ("vp%s.dat", strrep (num2str (w), '.', '_'));
fname_raw = @(w) sprintf ("rawdata_vp%s.dat", strrep (num2str (w), '.', '_'));

##
# We load the data form the two wind speeds we used
vp = 6;
for i=1:length(vp)
  load (fname_raw(vp(i)),'Temp', 'Hum', 'Pres','v','v_std');
  load (fname_vp(vp(i)));
  A         = 2 * radiusR * HR;
  A_std_rel = dr/radiusR + dH / HR;
   
  v_w{i}      = v;
  v_w_std_rel = v_std ./ v_w{i};
  
  vt{i}      = vtR_data;
  vt_std{i}  = vtR_data_std;
  
  tsr{i}     = vt{i} / v_w{i};
  tsr_std{i} = (vt_std{i} ./ vt{i} + v_w_std_rel) .* tsr{i};

  tau{i}     = - tauR - tauF(omegaR);
  tau_std{i} = tauR_std; % + tauF_std; # TODO

  rho{i} = air_density (Temp, Pres, Hum);

  Pw{i}      = 0.5 * A * v_w{i}^3 * rho{i};
  Pw_std_rel = 3 * v_w_std_rel + A_std_rel;
  
  Cp{i}     = tau{i} .* omegaR ./ Pw{i};
  Cp_std{i} = (tau_std{i} ./ tau{i} + omegaR_std ./ omegaR + Pw_std_rel) .* Cp{i};
endfor

## Plots
#
txt_label = @(v) {sprintf('%.1f m/s down',v), sprintf('%.1f m/s up',v)};
figure (1)
clf
h1  = errorbar (vt{1}, tau{1}, vt_std{1}, tau_std{1}, '~>o');
hold on
vtf = linspace (0, max (vt{1})*1.5, 100).';
h3 = plot (vtf, -tauF(vtf/radiusR),'k-');
hold off
axis tight
xlabel ('Tangential speed rotor [m/s]')
ylabel ('\tau_W [Nm]')
set (h1, 'markerfacecolor', 'auto')
legend ([h1; h3], {txt_label(v_w{1}){:}, '\tau_F'}, ...
'Location', 'NorthOutside','Orientation', 'horizontal');

print ('-dpng', 'TauW_vs_vt.png');
%print ('-dpng', 'TauW_vs_tsr.png');

figure (2)
clf
h1 = errorbar (tsr{1}, Cp{1}, tsr_std{1}, Cp_std{1}, '~>o');
axis tight
xlabel ('TSR [a.u.]')
ylabel ('C_p [a.u.]')
legend (h1, txt_label(v_w{1}), ...
'Location', 'NorthOutside','Orientation', 'horizontal');

print ('-dpng', 'Cp_vs_tsr.png');

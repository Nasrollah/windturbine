## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-02-02

## Script to collect and save measured data
#
fname = @(w) sprintf ("rawdata_vp%s.dat", strrep (num2str (w), '.', '_'));
vars  = {'vp', 'v', 'v_std', 'U', 'I', 'I_lu', 'fe', 'fe_err', 'Temp', 'Pres', 'Hum'};

## Voltage ramp up and down
# Last index is down (=1) and up (=2)
nU = 7;
U      = zeros (nU,2);
U(:,1) = [30 26 22 18 14 10 6].';
U(:,2) = flipud (U(:,1));

## Wind power 6 %
# Wind speed from anemometer @ 12.5 %
vp = 6;
v = 1.9;  # m/s
v_std = 0.1;

## 
# Current @ 6 %
I_lu        = zeros (nU,2,2);
I_lu(:,:,1) = [18.8 19.9;15.8 17.0; 12.2 13.6; 9 10; 7.3 7.8; ...
               5.7 6.3; 4.4 5.1] * 1e-2.';
;
I_lu(:,:,2) = [4.3 5.1; 6.2 6.8; 7.1 7.9; 9.2 10; 10.7 12; ...
              15.8 16.2; 19.4 20]*1e-2;
I           = squeeze (mean (I_lu, 2));

##
# Encoder rotation frequency
fe      = NA (nU,2);
fe(:,1) = [19.1; 16.5; 14.0; 11.5; 8.8; 6.3; 3.7];
fe(:,2) = [3.6; 6.3; 8.9; 11.4; 14; 16.6; 19.1].';
# Resolution in frequency measurement [Hz]
fe_err = 0.1; 

##
# Temprature, Pressure and Relative humidity
Pres = 978.1e2 * [1 1];  #Pa
Hum  = [0.52 0.53];
Temp = [11.7 10.3];     #C

fn = fname (vp);
save (fn, vars{:});
printf ("Saved to %s\n", fn); fflush (stdout);

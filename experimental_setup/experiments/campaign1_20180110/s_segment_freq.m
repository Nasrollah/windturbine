## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-11

fname_raw = @(vp,d) sprintf ("steadyW_%s_wind%s_speed.dat", d, ...
                          strrep (num2str(vp),'.','p'));
fname = @(vp) sprintf ("freq_vp%s.dat", strrep (num2str (vp), '.', '_'));
vars  = {'idx', 'freqR_data', 'freqR_data_std'};

vp     = 12.5;             # wind generator power percentage
direc  = {"down","up"};

np  = 8;  # numer of measurements
idx = zeros (np, 2, 2);
freqR_data = freqR_data_std = zeros (np, 2);

for j = 1:2
  d = direc{j};  # direction
  fn = fname_raw(vp,d);
  printf ("Load from %s\n", fn); fflush (stdout);
  data = load (fn);
  t = data(:,1);
  w = data(:,2);
  idx_o = 1:length(t);

  ##
  # Filter NA values or too large
  tf    = !isfinite (w) | w > 10;
  t(tf) = [];
  w(tf) = [];

  f = figure (1,'Name', num2str(vp));
  clf
  for i=1:np
    tmp = pickplot2d (f, t, w, '.');
    ww                  = w(tmp);
    freqR_data(i,j)     = mean (ww);
    freqR_data_std(i,j) = std (ww) / sqrt (length (ww));
    idx(i,:,j) = idx_o(tmp([1 end]));
  endfor

endfor
close (f);
fn = fname(vp);
save (fn, vars{:});
printf ("Saved to %s\n", fn); fflush (stdout);

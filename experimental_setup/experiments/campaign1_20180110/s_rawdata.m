## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-10

## Script to collect and save measured data
#
fname = @(w) sprintf ("rawdata_vp%s.dat", strrep (num2str (w), '.', '_'));
vars  = {'vp', 'v', 'v_std', 'U', 'I', 'I_lu', 'fe', 'fe_err', 'Temp', 'Pres', 'Hum'};

## Voltage ramp up and down
# Last index is down (=1) and up (=2)
U      = zeros (8,2);
U(:,1) = [30 26 22 18 14 10 6 2].';
U(:,2) = flipud (U(:,1));

## Wind power 12.5 %
# Wind speed from anemometer @ 12.5 %
vp = 12.5;
v = 5.0;  # m/s
v_std = 0.05;

## 
# Current @ 12.5 %
I_lu        = zeros (8,2,2);
I_lu(:,:,1) = [7 11; 11 16; 32 55; 60 70; 61 68; 49 55; 28 38; 8 16] ...
                  * 1e-3;
I_lu(:,:,2) = [8 14; 34 41; 48 59; 59 70; 59 73; 32 59; 16 22; 9 15] ...
                  * 1e-3;
I           = squeeze (mean (I_lu, 2));

##
# Encoder rotation frequency
fe      = NA (8,2);
fe(:,2) = [NA NA NA 8.97 11.5 14.2 17.0 19.7].';
# Resolution in frequency measurement [Hz]
fe_err = 0.1; 

##
# Temprature, Pressure and Relative humidity
Pres = 963.0e2 * [1 1];  #Pa
Hum  = [0.45 0.47];
Temp = [14.9 13.5];     #C

fn = fname (vp);
save (fn, vars{:});
printf ("Saved to %s\n", fn); fflush (stdout);

## Wind power 15 %
# Wind speed from anemometer @ 15 %
vp  = 15;
v = 6.0;  # m/s
v_std = 0.05;

## 
# Current @ 15 %
I_lu        = zeros (8,2,2);
I_lu(:,:,1) = [0 0;  1  8; 45 60; 54 66; 53 62; 34 44; 16 25; 0 0] ...
                 * 1e-3;
I_lu(:,:,2) = [0 1; 19 26; 36 42; 55 64; 56 69; 37 49;  8 14; 0 0] ...
                 * 1e-3;
I           = squeeze (mean (I_lu, 2));

##
# Encoder rotation frequency
fe      = NA (8,2);
fe(:,1) = [20.0 17.1 14.2 11.5 8.86 6.36 3.76 1.36].';
fe(:,2) = [1.3 3.8 6.4 9.08 11.6 14.3 17.1 19.9].';
# Resolution in frequency measurement [Hz]
fe_err = 0.1;

##
# Temprature, Pressure and Relative humidity
Pres = 963.0e2 * [1 1];  #Pa
Hum  = [0.49 0.51];
Temp = [12.2 11.4];     #C

fn = fname (vp);
save (fn, vars{:});
printf ("Saved to %s\n", fn); fflush (stdout);

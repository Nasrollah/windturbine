## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-11

fname = "freq_friction.dat";
vars  = {'idx', 'freqR', 'T'};

if !exist(fname, 'file')
  files = ls("friction_Urelax_*_speed.dat");
  nf  = size(files, 1);  # numer of files
  freqR = T = {};
  idx = [];
  for i = 1:nf
    fn = files(i,:);
    printf ("Load from %s\n", fn); fflush (stdout);
    data = load (fn);
    t = data(:,1);
    w = data(:,2);
    idx_o = 1:length(t);

    ##
    # Filter NA values or too large
    tf    = !isfinite (w) | w > 5;
    t(tf) = [];
    w(tf) = [];

    f = figure (1);
    clf
    axis tight
    tmp = 0;
    while true
      tmp          = pickplot2d (f, t, w, '.');
      if isempty (tmp); break; endif

      T{end+1}     = t(tmp) - t(tmp(1));
      freqR{end+1} = w(tmp);
      idx(end+1,:) = idx_o(tmp([1 end]));
    endwhile
  endfor
  close (f);
else
  load (fname);
endif
np = numel (T);
fmin = min(arrayfun(@(i)freqR{i}(1), 1:np));
## Plots
# Detect outliers
outlier = 6;

figure(1)
clf 
hold on
for i=1:np 
  j = find(freqR{i}<=fmin,1,'first');
  plot (T{i}-T{i}(j),freqR{i}, sprintf('o;%d;',i));
endfor
axis tight
xlabel ('Time [s]')
ylabel ('Freq [Hz]')

to_keep = setdiff (1:np, outlier);
T = T(to_keep);
freqR = freqR(to_keep);
idx = idx(to_keep,:);

save (fname, vars{:});
printf ("Saved to %s\n", fname); fflush (stdout);

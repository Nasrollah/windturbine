# Experiment steady state rotational speed

## Measured Veriables

- Rotational speed of the turbine: $`f`$ [Hz]
- Voltage in the power supply: $`U`$ [Volts]
- Current in the power supply: $`I`$ [Amps]
- Wind generator power index: $`P`$ [%]
- Wind speed (manual anemometer): $`v`$ [m/s]

## Description

In this experiment we use steady state measurements of: 1) voltage and current feed to the motor, 
and 2) rotational frequency of the rotor, to compute the performance coefficient 
via the estimation of torques exerted on the rotor of the turbine:

* Torque exerted by the motor, $`\tau_M`$.

* Torque due to electromechanical friction (motor is not fed power), $`\tau_F`$.

These two torques and the steady state condition can be used to infer the torque exerted by the wind on the rotor.
The calculation assumes that a rigid body rotation model is valid for the rotor 
and the following dependencies:

* $`\tau_M := \tau_M(U, I)`$

* $`\tau_F := \tau_M(f)`$

## Experiments

### Rotational speed at constant wind (12.5%), decreasing motor voltage
Data stored in `steadyW_down_wind12p5_*.dat`

#### Starting conditions:
 - Power supply voltage:        30 V 
 - Power suply current:         0.010 A
 - Air pressure                 963.0 hPa
 - Air relativ humidity         45 %
 - Air temperatur               14.9 °C
 - Stable rotation speeed       4.55 Hz (sensor), 19.7 (encoder)
 - Wind speed index             12.5
 - Wind speed anemometer        5.0 m/s

#### Notes
- The current fluctuated in the beginning between 0.007-0.011 A.

#### Voltage 
U = [30 26 22 18 14 10 6 2].';

#### Current
I = [0.007 0.011; 0.011 0.016; 0.032 0.055; 0.060 0.070; 0.061 0.068; 0.049 0.055; 0.028 0.038; 0.008 0.016];

### Rotational speed at constant wind (12.5%), increasing motor voltage
Data stored in `steadyW_up_wind12p5_*.dat`

#### Starting conditions:
 - Power supply voltage:        2 V 
 - Power suply current:         0.011 A
 - Air pressure                 963.0 hPa
 - Air relativ humidity         47 %
 - Air temperatur               13.5 °C
 - Stable rotation speeed       0.285 Hz (sensor), 1.2 (encoder)
 - Wind speed index             12.5
 - Wind speed anemometer        5.0 m/s

#### Notes
- The current fluctuated in the beginning between 0.008-0.014 A.

#### Voltage 
U = [2 6 10 14 18 22 26 30].';

#### Current
I = [0.008 0.014; 0.034 0.041; 0.048 0.059; 0.059 0.070; 0.059 0.073; 0.032 0.059; 0.016 0.022; 0.009 0.015];

#### Encoder frequency
fe = [NA NA NA 8.97 11.5 14.2 17.0 19.7].';

### Rotational speed at constant wind (15%), decreasing motor voltage
Data stored in `steadyW_down_wind15_*.dat`

#### Starting conditions:
 - Power supply voltage:        30 V 
 - Power suply current:         0.00 A
 - Air pressure                 963.0 hPa
 - Air relativ humidity         49 %
 - Air temperatur               12.2 °C
 - Stable rotation speeed       4.65 Hz (sensor), 20.0 (encoder)
 - Wind speed index             15.0
 - Wind speed anemometer        6.0 m/s

#### Notes
- The current is zero, speed looks stable.

#### Voltage 
U = [30 26 22 18 14 10 6 2].';

#### Current
I = [0.000 0.000;0.001 0.008;0.045 0.060;0.054 0.066;0.053 0.062;0.034 0.044; 0.016 0.025; 0.000 0.000];

#### Encoder frequency
fe = [20.0 17.1 14.2 11.5 8.86 6.36 3.76 1.36].';

### Rotational speed at constant wind (15%), increasing motor voltage
Data stored in `steadyW_up_wind15_*.dat`

#### Starting conditions:
 - Power supply voltage:        2 V 
 - Power suply current:         0.00 A
 - Air pressure                 963.0 hPa
 - Air relativ humidity         51 %
 - Air temperatur               11.4 °C
 - Stable rotation speeed       0.3 Hz (sensor), 1.3 (encoder)
 - Wind speed index             15.0
 - Wind speed anemometer        6.0 m/s

#### Notes
- The current is zero, speed looks stable.

#### Voltage 
U = [2 6 10 14 18 22 26 30].'

#### Current
I = [0.000 0.001; 0.019 0.026; 0.036 0.042; 0.055 0.064; 0.056 0.069; 0.037 0.049; 0.008 0.014; 0.000 0.000];

#### Encoder frequency
fe = [1.3 3.8 6.4 9.08 11.6 14.3 17.1 19.9].';

### Equilibirum rotational speed for different motor torques and no wind
Data stored in `friction_Uramp_*.dat`

Ramp voltage up and down, registering speed and current

#### Notes
- At 30-26 V the speed is unreliable. Seems like electrical interferences

#### Voltage 
Uup = [2 6 10 14 18 22 26 30].';
Udw = [30 26 22 18 14 10 6 2];

#### Current
Iup = [29 42; 45 65; 78 86; 104 113; 133 145; 155 187; 196 211; 234 244] * 1e-3;
Idw = [221 232; 195 205; 158 173; 129 140; 104 110; 77 85; 51 64; 32 42] * 1e-3;

#### Encoder frequency
feup = [1.1 3.7 6.3   8.8   11.4  14.0  16.5  23].';
fedw = [100  45 13.99 11.39 8.809 6.209 3.609 1.109].';

### Relaxation of rotational speed from high initial value and no wind, 1st modality

Data stored in `friction_Urelax_0_*.dat`

Set maximum voltage and let the turbine go down to rest.
The power supply controller is set to off but the motor is still
connected to it.
#### Notes
- At 30 V the speed is unreliable. Seems like electrical interferences
- 3rd go up is with jumps of 10 V and down not off but set 0 V
- 4th go up is with jumps of 1 V and down not off but set 0 V

#### Voltage 
U = [30 30 30 30].';

#### Current
I = [222 233; 227 238; 227 239; 227 235] * 1e-3;

#### Encoder frequency
fe = [19 120 100 90].';

### Relaxation of rotational speed from high initial value and no wind, 2nd modality

Data stored in `friction_Urelax_1_*.dat`

Set maximum voltage and let the turbine go down to rest.
The power supply controller is set to off but the motor is still
connected to it.
#### Notes
- 1st go up with steps of 0.1 V
- 2nd go up direct to max, then touch cables, no current nor encoder measurements

#### Voltage 
U = [30 30].';

#### Current
I = [220 232; NA NA] * 1e-3;

#### Encoder frequency
fe = [20; NA].';

### Relaxation of rotational speed from high initial value and no wind, 3rd modality
Data stored in `friction_Urelax_2_*.dat`

Set maximum voltage and let the turbine go down to rest.
The power supply controller is disconnected.
(restart of the pi)

#### Voltage 
U = 30;

#### Current
I = [216 235] * 1e-3;

#### Encoder frequency
fe = 19.09;


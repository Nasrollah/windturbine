## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-11

fname_data = "freq_friction.dat";
load (fname_data);

fname = 'tauF.dat';
vars  = {'tauF', 'J', 'dJ', 'pwdot', 'pTau_J','wmax'};

##
# synchronize

np = numel (T);
fmin = min(arrayfun(@(i)freqR{i}(1), 1:np));
t = w = [];
for i=1:np
  j = find (freqR{i} <= fmin, 1, 'first');
  t = [t; T{i}-T{i}(j)]; 
  w = [w; freqR{i}];
endfor
w *= 2 * pi;
[t,o] = sort (t);
w = w(o);
w = w;
pw     = splinefit (t, w, 6, 'robust', true);
pwdot  = ppder (pw);
%pt     = splinefit (w, t, 6, 'robust', true);
%tauF_J = @(w) ppval (pwdot, ppval(pt, w));
pTau_J  = splinefit (w, ppval (pwdot, t), 6, 'robust', true);
tauF_J = @(w) ppval (pTau_J, w);

##
# properties of gearbox installed in motor
gearGP52 = struct ('ratio', 4.3, 'efficiency', 0.91);
radiusR  = 0.35; # in meters

U(:,1) = [2 6 10 14 18 22 26 30].';
U(:,2) = [30 26 22 18 14 10 6 2];

I_lu(:,:,1) = [29 42; 45 65; 78 86; 104 113; 133 145; 155 187; 196 211; 234 244] * 1e-3;
I_lu(:,:,2) = [221 232; 195 205; 158 173; 129 140; 104 110; 77 85; 51 64; 32 42] * 1e-3;
I = squeeze (mean (I_lu, 2));

fe(:,1) = [1.1 3.7 6.3   8.8   11.4  14.0  16.5  23].';
fe(:,2) = [100  45 13.99 11.39 8.809 6.209 3.609 1.109].';

# Error of the mean from interval
# Measurements are consider 0.99 confidence intervals
coeff = 1 ./ erfinv (0.99);
lu2std = @(LU) squeeze (std (LU, 0, 2)) * coeff;

# Motor/rotor torque and rotation
[TM, WM, TR, WR]         = maxonRE50_model (U, I, gearGP52);
[TMlu, WMlu, TRlu, WRlu] = maxonRE50_model (U, lu2std(I_lu), gearGP52);

tauM = TM; tauM_std = TMlu;
tauR = TR; tauR_std = TRlu;
omegaM = WM; omegaM_std = WMlu;
omegaR = WR; omegaR_std = WRlu;

# moment of inertia

%J_loc   = - tauR ./ tauF_J(omegaR);
% J       = mean (J_loc(:));

[pJ sJ] = polyfit (- tauF_J(omegaR(:)), tauR(:), 1);
J       = pJ(1);
dJ      = sqrt (diag (sJ.C) / sJ.df)(1) * sJ.normr;

%tauF  = @(w) J * ppval (pwdot, ppval(pt, w));
wmax = max (omegaR(:));
tauF  = @(w) J * ifelse(w<=wmax, ppval (pTau_J, w), ...
                 interp1(wmax.*[0.95 1], ...
                 ppval(pTau_J,wmax.*[0.95 1]), w,'extrap'));

## Plots
wf    = linspace (0, max(w)*1.1, 100);
figure (1);
plot (t, w, 'or', t, ppval(pw, t), 'k-'); %, ppval(pt, wf), wf, 'g-');
axis tight
xlabel ('Time [s]')
ylabel ('\omega [rad/s]')

figure(2)
tF_J = -tauF_J (omegaR(:));
tf_J = [min(tF_J); max(tF_J)];
Jf   = polyval (pJ, tf_J);
plot (-tauF_J(omegaR), tauR, 'o', tf_J, Jf, 'r-');
axis tight
ylabel ('\tau_F/J [rad/s²]')
xlabel ('\tau_R [Nm]')

figure(3)
plot (wf, tauF(wf), 'k-', omegaR, -tauR, 'o');
axis tight
ylabel ('\tau_F [Nm]')
xlabel ('\omega [rad/s]')

#figure(4)
#plot (omegaR, J_loc, 'o');
#line([min(omegaR(:)) max(omegaR(:))], J,'color','b')
#line([min(omegaR(:)) max(omegaR(:))], mean(J_loc(:,1)),'color','r')
#line([min(omegaR(:)) max(omegaR(:))], mean(J_loc(:,2)),'color','r')

axis tight
ylabel ('J [kg m²]')
xlabel ('\omega [rad/s]')

save (fname, vars{:});
fprintf ("Saved to %s\n", fname);



Comment block 1: license, e.g. GPLv3

Comment block 2: author, e.g. 
   ## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

Comment block 2: help of the function
  1. Matlab help style
  2. Octave help style, uses texinfo
 
 
function [list of output args] = function name ( list of input args )

  function code here!

end%function

Comment blocks: Tests (unit tests)

Optional Comment blocks: Demos (example how to use function, with plots if possible)


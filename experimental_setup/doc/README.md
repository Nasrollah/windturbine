# Experimental setup description

## Hardware
### Torque sensor
See [TRANSMETRA DR2112R_5](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/TorqueSensor_DR2112R_5.pdf) for details

Description | Details | Cable color
------------|---------|------------
Vcc            |15V (DC) | RED
Vcc-GND        |         | BLUE
Signal: torque |[-5,5]V  | PINK
Signal: GND    |         | GRAY

#### Connection

[AD8475](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/AD8475_specs.pdf), 0.8x, output for each channel: [0.5,4.5]V.
The actual measurement is (OUT+ - OUT-), which gives [-4,4]V.

[MCP3208](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/MCP3208_specs.pdf).

See [torqueSensor_AD8475_RaspberryPi_schema.pdf](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/ExpSetup_Connection_Schema.pdf) for details.

### Rotational speed sensor

[Encoder HEDS 5540](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/Maxon_Encoder_HEDS5540_500CPT_3Channels.pdf)

Description | Details | Cable color | Notes
------------|---------|-------------|------
Vcc         | 5V(DC)  | RED         | 
Vcc-GND     |         | BLACK       | 
Signal A    | 5V TTL  | GREEN       | 500 pulses per motor shaft revolution 
Signal I    | 5V TTL  | YELLOW      | 1 pulse per motor shaft revolution 

Signal A is too fast to reliably measure it with the Raspberry Pi.
Signal I can be measured using GPIO ports.

For any of the signals a [voltage limiter](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/ExpSetup_Connection_Schema.pdf) is needed since GPIO ports
are meant for 3.3V digital signals.

The signal is after [the gears](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/Maxon_PlanetaryGearhead_GP52_52mm_4-30Nm.pdf) hence it is not the rotational speed of the turbine but of the motor shaft.

### Rotational speed control
Motor [Maxon RE50](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/doc/Maxon_RE50_50mm_GraphiteBrushes_200W.pdf)

## Software
The data aquisition software is organized as follows: 

1. a logger for a single sensor reads a data value and save it to a file, then it publishes the data to a specified port.

2. a plotter reads the specified hostname:port and generates plots of the data.

The idea is that the Raspberry will not be used for plotting, just for logging.
An external compter should be used to visualize the current status of the measurements.

Some bash scripts are provided as examples of use:

* [launch_loggers.sh](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/data_logger/launch_loggers.sh) launches all loggers.
* [launch_plotters.sh](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/data_logger/launch_plotters.sh) launches all plotters.
* [lognplot.sh](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/data_logger/lognplot.sh) launches all loggers and plotters.


### Loggers
We provide loggers for:

* Unipolar torque
* Bipolar torque
* Rotational speed

See [logger_single.py](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/data_logger/logger_single.py) for details.

The loggers are implemneted as a single function that depends on [numpy]() and [zeromq]().

To run excecute
```
python logger_single.py -s <sensor> -f <frequency> -o <output file>
```
(use option --help to get help)

The logger type is chosen at run time with command line arguments (-s or --sensor). It can be T for unipolar torque, T2 for bipolar torque, and W for rotational speed.

The desired sampling frequency in Hz is set using the -f or --frequency option (the hardware might not be able to reach frequnecies above 200 Hz). 
The default is 100 Hz for the torque logger and 50 Hz for the rotatioanl speed logger.

If the option -o or --output is provided with a path to a filename, then the measured data will be recorded to that file. If no option is given data is written to /dev/null.

### Plotters
We provide [matplotlib]() based plotters for:

* Torque
* Rotational speed

See [plotter_single.py](https://gitlab.com/kakila/windturbine_HSR/blob/master/experimental_setup/data_logger/plotter_single.py) for details.

To run excecute
```
python plotter_single.py -s <sensor> -d <deque size> -H <hostname or ip>
```
(use option --help to get help)

The plotter type is chosen at run time with command line arguments (-s or --sensor). It can be T for uni/bipolar torque and W for rotational speed.

The plots use deques to show the data, the argument -d or --dequesize set the length of the deque.
For example if a logger publishes data at 10Hz a deque with size 100 will plot of the last 10 seconds of data.

The plotters can be used in the Raspberry Pi, but at risk of reducing the synchronization of the measured and logged data.
We also observed that when all loggers and plotters are active the Pi overheats.

To plot from a remote computer (assuming that the ip of the raspberry is reachable and that ports are open. These assumptions are OK if the pia nd the plotting computer are in the same LAN subnet) use the option -H or --hostname. For exmaple

```
python plotter_single.py -s W -d 500 -H 192.168.178.29
```

will plot the data published by a rotation sensor on a machine with ip 192.168.178.29


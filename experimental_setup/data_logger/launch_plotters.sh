x-terminal-emulator --geometry=50x8 \
-t "Ang. speed plotter" \
-e "python plotter_single.py -s W" &

x-terminal-emulator --geometry=50x8 \
-t "Torque plotter"  \
-e "python plotter_single.py -s T" &

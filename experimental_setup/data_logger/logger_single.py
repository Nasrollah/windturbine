'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import zmq
import sys

import numpy as np
try:
  import RPi.GPIO as GPIO
except ImportError:
  print ("warning: Rpi.GPIO not available")
  pass

from time import time, sleep
from datetime import datetime
from itertools import count
try:
  from gpiozero import MCP3208
except ImportError:
  print ("warning: GPIO0 not available")
  pass

TORQUE = 0
SPEED  = 1
SIGNAL = 2

PORT = 5556

class Sensor (object):

    def __init__(self, label='', buffsz=1):
      self.key    = None
      self.value  = 0.0
      self.label  = label
      self.buffsz = buffsz
      self.__values__ = lambda n: np.zeros(n,)
      
    def update_value (self):
      return self.value

class Torque (Sensor):

    def __init__(self, **kwargs):
      super(Torque, self).__init__(**kwargs)
      self.key    = TORQUE
      self.adc    = MCP3208(channel=0)
      self.Vcc    = 5.0 # volts
      self.scale  = self.Vcc / float(self.buffsz)
      
    def update_value (self):
      self.value = 0.0
      for i,v in enumerate(self.adc.values):
        if i > self.buffsz-1:
          break
        self.value += v
        
      self.value *= self.scale
      return  self.value

class TorqueBi (Sensor):

    def __init__(self, **kwargs):
      super(TorqueBi, self).__init__(**kwargs)
      self.key    = TORQUE
      self.adc_up = MCP3208(channel=0)
      self.adc_dn = MCP3208(channel=1)
      self.Vcc    = 5.0 # volts
      self.scale  = self.Vcc / float(self.buffsz)
      
    def update_value (self):
      self.value = 0.0
      # TODO use values iterators
      # this is not working
      #for i,vu,vd in enumerate(zip(self.adc_up.values,self.adc_dn.values)):
      #  if i > self.buffsz-1:
      #    break
      for i in range(self.buffsz):
        self.value += (self.adc_up.value - self.adc_dn.value)
        
      self.value *= self.scale
      return  self.value

class RotSpeed (Sensor):

    def __init__(self, **kwargs):
      super(RotSpeed, self).__init__(**kwargs)

      self.key     = SPEED
      self.ch      = 13
      GPIO.setmode(GPIO.BOARD)
      GPIO.setup(self.ch, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
      self.scale   = 1.0 / 4.3 # gear ratio
      
    def update_value(self):
      self.value = time()
      for i in range(1):
        st = GPIO.wait_for_edge(self.ch, GPIO.RISING, timeout=5000)
      if st:
        freq = self.scale / (time() - self.value)
        if freq <= 30:
          self.value = freq
        else:
          self.value = np.nan
      else:
        self.value = np.nan

      return self.value

class Signal (Sensor):
    _ids = count(0)

    def __init__(self, t0=0, **kwargs):
      super(Signal, self).__init__(**kwargs)
      self.key    = SIGNAL*10 + next(self._ids)
      self.t  = 0 + t0
      self.dt = 0.1
      self.update_value()

    def update_value(self):
      self.t += self.dt
      self.value = np.sin (2*np.pi*self.t)
      return self.value

def zmq_logger (outputfile, sensor, port = PORT, freq=100):
    '''
     Logger using zmq to broadcast data

     Based on weather server http://zguide.zeromq.org/py:wuserver
    '''
    if outputfile.lower() == 'null':
      from os import devnull
      outputfile = devnull

    port = 'tcp://*:{}'.format(port + sensor.key)
    print('\n## Wind turbine HSR looger ##')
    # Connection
    context = zmq.Context()
    socket  = context.socket(zmq.PUB)
    socket.set_hwm(int(10*freq))

    socket.bind(port)
    print('Socket initialized!')
    sensor.update_value()
    print('{} [{}] sensor initialized!'.format(sensor.label,sensor.key))
    print('Publishing data on : {}'.format(port))
    print('Writting to file   : {}'.format(outputfile))
    print('Sampling rate [Hz] : {}'.format(freq))

    dt_     = 1.0 / freq
    dt      = dt_
    T       = 0.0
    samp    = 0
    start_t = time()
    buffer  = ''
    with open(outputfile, 'w') as fout:
        # header
        fout.write(u'# Wind turbine HSR looger\n')
        fout.write(u'# Automatically created on {}\n'.format(datetime.now()))
        fout.write(u'# Desired sampling rate {} Hz, data is not uniformly sampled\n'.format(freq))
        fout.write(u'# Time [s], {}\n'.format(sensor.label))

        vals = [''] * 2
        live = ''
        while True:
          dtime = time()
          vals[1] = '{}'.format(sensor.update_value())
          vals[0] = '{}'.format(time() - start_t)
          socket.send_multipart([b"{}".format(sensor.key),
                                b" ".join(vals[-2:])])
            
          buffer += (u' '.join(vals) + u'\n')

          if dt > 0:
             sleep (dt)

          T += time() - dtime
          samp += 1
          if T > 0.5 or samp >= 100:
            dte = T / samp
            dt  += (dt_ - dte)
            live = '*' if live is ' ' else ' '
            sys.stdout.write('\rEstimated rate [Hz]: {:.1f}\t{}'.
                                                  format(1.0/dte, live))
            sys.stdout.flush()

            fout.write(buffer)
            fout.flush()

            buffer = ''
            T      = 0.0
            samp   = 0 

if __name__ == "__main__":
    import getopt
    def parse_options (args):
        hlp_str = 'logger.py -o <output file> -f <sampling freq> -s <sensor: T,W>'
        options = {'outputfile':'Null'}
        try:
          opts, args = getopt.getopt(args,"ho:f:s:",\
                                           ["outputfile=",\
                                            "frequency=", \
                                            "sensor"])
        except getopt.GetoptError:
          print (hlp_str)
          sys.exit(2)
        for opt, arg in opts:
          if opt == '-h':
            print (hlp_str)
            sys.exit()
          elif opt in ("-o", "--outputfile"):
            options['outputfile'] = arg
          elif opt in ("-f", "--frequency"):
            options['freq'] = float(arg)
          elif opt in ("-s", "--sensor"):
            if arg == "T":
              options['sensor'] = Torque (label='Torque [mNm]')
            elif arg == "T2":
              options['sensor'] = TorqueBi (label='Torque(Bi) [mNm]')
            elif arg == "W":
              options['sensor'] = RotSpeed (label='Ang. speed [Hz]')
              if 'freq' not in options.keys():
                options['freq'] = 50

        return options

    opts = parse_options(sys.argv[1:])
    zmq_logger(**opts)

'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# This file is based on subplots.py of matplotlib
# and zmq weather client

import sys
import matplotlib
import zmq

matplotlib.use('TkAgg')

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.animation import FuncAnimation
from collections import deque
from time import time, sleep

class plotter (object):

  def __init__ (self, hostname, port, key, label, **kwargs):
    print('\n## Wind turbine HSR {} plotter ##'.format(label))

    self.key = u'{}'.format(key)
    # Data communication initialization
    sys.stdout.write('Initializing 0MQ socket ...')
    sys.stdout.flush()
    #  Socket to talk to server
    self.context = zmq.Context()
    self.socket  = self.context.socket(zmq.SUB)
    addr         = 'tcp://{}:{}'.format(hostname, port)
    self.socket.connect(addr)
    self.socket.setsockopt_string(zmq.SUBSCRIBE, self.key)
    print(' done!')
    print('Listening to: {}[{}]'.format(addr, self.key))

    # Plot initialization
    sys.stdout.write('Initializing matplotlib figure ...')
    sys.stdout.flush()

    self.label   = label
    self.blit    = False
    self.fig     = plt.figure()
    self.ax      = self.fig.add_subplot(2, 1, 1)
    self.ax_text = self.fig.add_subplot(2, 1, 2)
    print(' done!')

    self.start_t  = time()
    self.lstframe = time()

    self.nT     = kwargs['DSize']
    self.ndata  = 10
    self.t      = deque([np.nan]*self.nT, self.nT)

    self.data   = deque([0] * self.nT, self.nT)

    sys.stdout.write('Initializing matplotlib plots ...')
    sys.stdout.flush()

    self.text_tmpl    = {
    'time': 'Elapsed time [s]: {:.2f}',
    'lastup': 'Timestamp data [s]: {:.2f}',
    'fps': 'Frames per second: {:.1f}',
    'ndata': 'Points per frame: {:.1f}'
    }
    self.text         = dict()
    self.text['time'] = self.ax_text.text(
        0, 0.85, self.text_tmpl['time'].format(0),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['lastup'] = self.ax_text.text(
        0, 0.70, self.text_tmpl['lastup'].format(0),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['fps'] = self.ax_text.text(
        0, 0.55, self.text_tmpl['fps'].format(5),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['ndata'] = self.ax_text.text(
        0, 0.40, self.text_tmpl['ndata'].format(self.ndata),
        transform=self.ax_text.transAxes, animated=self.blit)

    self.plt, = self.ax.plot(
          self.t, self.data,
          linestyle='-', marker='o', markeredgecolor='r',  markerfacecolor='None',
          markersize=2, animated=self.blit)

    self.artists = [self.plt] + self.text.values()
    print(' done!')

  def init_plot (self):
    sys.stdout.write('Fetching 1st data batch ...')
    sys.stdout.flush()

    self.fetch_data()
    print(' done!')

    self.t0 = self.t[-1]

    sys.stdout.write('Decorating plot ...')
    sys.stdout.flush()

    self.ax.set_ylabel(self.label)
    self.ax.grid(True, which='both')

    self.ax_text.set_axis_off()
    self.ax_text.set_frame_on(False)
    print(' done!')

    return self.artists

  def fetch_data(self):
    # Get data
    i = 0
    data_dt = 0
    while i < self.ndata:
        [key, string] = self.socket.recv_multipart()
        datstr        = string.split()
        dtime         = float(datstr[0])
        if dtime > self.t[-1] or np.isnan(self.t[-1]):
          self.t.append(dtime)

        self.data.append(float(datstr[1]))
        if i == 0:
            data_dt = self.t[-1]
        i += 1

    data_dt = self.t[-1] - data_dt
    return data_dt

  def update_plot(self):
    # Update lines
    t = map (lambda x: x - self.t0, self.t)
    self.plt.set_data(t, self.data)

    # Update axes
    # recompute the ax.dataLim
    self.ax.relim()
    # update ax.viewLim using the new dataLim
    self.ax.autoscale_view(scalex=False)
    self.ax.autoscale_view(tight=True, scaley=False)

  def update (self, frame):
    # Update data
    fetch_time = time()
    data_dt = self.fetch_data()
    fetch_time = time() - fetch_time

    # Update graphs
    self.update_plot()

    # Update text
    self.text['time'].set_text(self.text_tmpl['time'].format(time() - self.start_t))
    self.text['lastup'].set_text(self.text_tmpl['lastup'].format(self.t[-1]))
    self.text['ndata'].set_text(self.text_tmpl['ndata'].format(self.ndata))

    # Update amount of data requested to logger to match realtime
    dt          = time() - self.lstframe - fetch_time
    try:
      self.ndata *= dt / data_dt
    except ZeroDivisionError:
      pass

    self.text['fps'].set_text(self.text_tmpl['fps'].format(1.0/dt))

    self.lstframe = time()

    return self.artists

  def run (self):
    self.ani  = FuncAnimation(self.fig, self.update, init_func=self.init_plot,
                              frames=None, interval=100,
                              blit=self.blit)
    plt.show()
    return

if __name__ == "__main__":
  import getopt
  from logger_single import TORQUE, SPEED, PORT

  def parse_options (args):
    hlp_str = 'plotter.py -d <deque size> -s <sensor : T,W> -H <hostname: hostname or ip>'
    options = {'DSize':1000, 'hostname': 'localhost'}
    try:
      opts, args = getopt.getopt(args,"hd:s:H:",["dequesize=", 'sensor='])
    except getopt.GetoptError:
      print (hlp_str)
      sys.exit(2)
    for opt, arg in opts:
      if opt in ("-h", "--help"):
        print (hlp_str)
        sys.exit()
      elif opt in ("-d", "--dequesize"):
        options['DSize'] = int(arg)
      elif opt in ("-s", "--sensor"):
        if arg == "T":
          options['key'] = TORQUE
          options['label'] = 'Torque'
          options['port'] = PORT + TORQUE
        elif arg == "W":
          options['key'] = SPEED
          options['label'] = 'Ang. speed'
          options['port'] = PORT + SPEED
      elif opt in ("-H", "--hostname"):
          options['hostname'] = arg
    return options

  opts = parse_options(sys.argv[1:])
  pp = plotter(**opts)
  pp.run()

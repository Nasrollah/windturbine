DEV_W="${1}_speed.dat"
DEV_T="${1}_torque.dat"
if [ $# -eq 0 ]
  then
	DEV_T="Null"
	DEV_W="Null"
fi

x-terminal-emulator --geometry=80x8 \
-t "Ang. speed logger" \
-e "python logger_single.py -s W -o ${DEV_W}" &

x-terminal-emulator --geometry=80x8 \
-t "Torque logger"  \
-e "python logger_single.py -s T2 -o ${DEV_T}" &

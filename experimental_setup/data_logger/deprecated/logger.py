'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import zmq
import sys

import numpy as np
try:
  import RPi.GPIO as GPIO
except ImportError:
  print ("warning: Rpi.GPIO not available")
  pass

from time import time, sleep
from datetime import datetime
from itertools import count
try:
  from gpiozero import MCP3208
except ImportError:
  print ("warning: GPIO0 not available")
  pass

TORQUE = 0
SPEED  = 1
SIGNAL = 2

class Sensor (object):

    def __init__(self, label='', buffsz=1):
      self.key    = None
      self.value  = 0.0
      self.label  = label
      self.buffsz = buffsz
      self.__values__ = lambda n: np.zeros(n,)
      
    def update_value (self):
      self.value = np.mean(self.__values__(self.buffsz))
      return self.value

class Torque (Sensor):

    def __init__(self, **kwargs):
      super(Torque, self).__init__(**kwargs)
      self.key    = TORQUE
      self.adc    = MCP3208(channel=0)
      self.__values__ = self.torque

      def torque (self,n):
        val = []
        for i,v in enumerate(self.adc.values):
          val.append(v)
          if i >= n-1:
            break
        return val

class RotSpeed (Sensor):

    def __init__(self, **kwargs):
      super(RotSpeed, self).__init__(**kwargs)

      self.key     = SPEED
      self.ch      = 13
      GPIO.setmode(GPIO.BOARD)
      GPIO.setup(self.ch, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
      self.__values__ = self.rps
      self.time       = time()
      self.update_value()
      
    def rps(self, n):
      val = []
      for i in range(n):
        if GPIO.wait_for_edge(self.ch, GPIO.RISING, timeout=5):
          val.append(1.0 / (time() - self.time))
          self.time = time()
      # If no edge was detected retrun old value
      if len(val) == 0:
        val = [self.value]
        self.time = time()
      return val
      
    def add_counter(self,ch):
      self.counter += 1


class Signal (Sensor):
    _ids = count(0)

    def __init__(self, t0=0, **kwargs):
      super(Signal, self).__init__(**kwargs)
      self.key    = SIGNAL*10 + next(self._ids)
      self.t  = 0 + t0
      self.dt = 0.1
      self.update_value()

    def update_value(self):
      self.t += self.dt
      self.value = np.sin (2*np.pi*self.t)
      return self.value

def zmq_logger (outputfile, s_array, port = "tcp://*:5556", freq=100):
    '''
     Logger using zmq to broadcast data

     Based on weather server http://zguide.zeromq.org/py:wuserver
    '''
    if outputfile.lower() == 'null':
      from os import devnull
      outputfile = devnull
      
    print('\n## Wind turbine HSR looger ##')
    # Connection
    context = zmq.Context()
    socket  = context.socket(zmq.PUB)
    # TODO control frame rate with zmq.RATE?
    #socket.setsockopt_string(zmq.RATE, ...)
    socket.set_hwm(int(10*freq))

    socket.bind(port)
    print('Socket initialized!')
    for s in s_array:
      s.update_value()
      print('{} [{}] sensor initialized!'.format(s.label,s.key))
    print('Publishing data on : {}'.format(port))
    print('Writting to file   : {}'.format(outputfile))
    print('Sampling rate [Hz] : {}'.format(freq))

    dt_     = 1.0 / freq
    dt      = dt_
    T       = 0.0
    samp    = 0
    start_t = time()
    buffer  = ''
    with open(outputfile, 'w') as fout:
        # header
        fout.write(u'# Wind turbine HSR looger\n')
        fout.write(u'# Automatically created on {}\n'.format(datetime.now()))
        fout.write(u'# Desired sampling rate {} Hz, data is not uniformly sampled\n'.format(freq))
        labels = ['{}'.format(x.label) for x in s_array]
        fout.write(u'# Time {}\n'.format(' Time '.join(labels)))

        vals =['']*2*len(s_array)
        live = ''
        while True:
            dtime = time()
            for i,s in enumerate(s_array):
              vals[2*i+1] = '{}'.format(s.update_value())
              vals[2*i]   = '{}'.format(time() - start_t)
              socket.send_multipart([b"{}".format(s.key),
                                b" ".join(vals[-2:])])
            
            buffer += (u' '.join(vals) + u'\n')

            if dt > 0:
                sleep (dt)

            T += time() - dtime
            samp += 1
            if T > 0.5 or samp >= 100:
                dte = T / samp
                dt  += (dt_ - dte)
                live = '*' if live is ' ' else ' '
                sys.stdout.write('\rEstimated rate [Hz]: {:.1f}\t{}'.
                                                  format(1.0/dte, live))
                sys.stdout.flush()

                fout.write(buffer)
                fout.flush()

                buffer = ''
                T      = 0.0
                samp   = 0


if __name__ == "__main__":
    import getopt
    def parse_options (args):
        hlp_str = 'logger.py -o <output file> -f <sampling freq>'
        options = {'outputfile':'Null'}
        try:
          opts, args = getopt.getopt(args,"ho:f:",["outputfile=","frequency="])
        except getopt.GetoptError:
          print (hlp_str)
          sys.exit(2)
        for opt, arg in opts:
          if opt == '-h':
            print (hlp_str)
            sys.exit()
          elif opt in ("-o", "--outputfile"):
            options['outputfile'] = arg
          elif opt in ("-f", "--frequency"):
            options['freq'] = float(arg)

        return options

    opts = parse_options(sys.argv[1:])
    opts['s_array'] = (
                       Signal(label='Torque'),
                       RotSpeed(label='Ang. speed')
                       )
    zmq_logger(**opts)

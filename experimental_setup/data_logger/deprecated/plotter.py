'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

# This file is based on subplots.py of matplotlib
# and zmq weather client

import sys
import matplotlib
import zmq

matplotlib.use('TkAgg')

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.animation import FuncAnimation
from collections import deque
from time import time, sleep

class plotter (object):

  def __init__ (self, **kwargs):
    print('\n## Wind turbine HSR plotter ##')

    # Data communication initialization
    sys.stdout.write('Initializing 0MQ socket ...')
    sys.stdout.flush()
    #  Socket to talk to server
    self.context = zmq.Context()
    self.socket  = self.context.socket(zmq.SUB)
    self.socket.connect("tcp://localhost:5556")
    self.socket.setsockopt_string(zmq.SUBSCRIBE, u'20')
    self.socket.setsockopt_string(zmq.SUBSCRIBE, u'1')
    print(' done!')

    # Plot initialization
    sys.stdout.write('Initializing matplotlib figure ...')
    sys.stdout.flush()

    self.blit = False
    self.fig = plt.figure()
    self.ax_torque = self.fig.add_subplot(3, 1, 1)
    self.ax_speed  = self.fig.add_subplot(3, 1, 2)
    self.ax_text   = self.fig.add_subplot(4, 2, 7)
    print(' done!')

    self.start_t  = time()
    self.lstframe = time()

    self.nT     = kwargs['DSize']
    self.ndata  = 10
    self.t      = deque([np.nan]*self.nT, self.nT)

    self.torque = deque([0] * self.nT, self.nT)
    self.speed  = deque([0] * self.nT, self.nT)

    sys.stdout.write('Initializing matplotlib plots ...')
    sys.stdout.flush()

    self.text_tmpl    = {
    'time': 'Elapsed time [s]: {:.2f}',
    'lastup': 'Timestamp data [s]: {:.2f}',
    'fps': 'Frames per second: {:.1f}',
    'ndata': 'Points per frame: {:.1f}'
    }
    self.text         = dict()
    self.text['time'] = self.ax_text.text(
        0, 0.85, self.text_tmpl['time'].format(0),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['lastup'] = self.ax_text.text(
        0, 0.70, self.text_tmpl['lastup'].format(0),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['fps'] = self.ax_text.text(
        0, 0.55, self.text_tmpl['fps'].format(5),
        transform=self.ax_text.transAxes, animated=self.blit)
    self.text['ndata'] = self.ax_text.text(
        0, 0.40, self.text_tmpl['ndata'].format(self.ndata),
        transform=self.ax_text.transAxes, animated=self.blit)

    self.torque_plt, = self.ax_torque.plot(
          self.t, self.torque,
          linestyle='-', marker='o', markeredgecolor='r',  markerfacecolor='None',
          markersize=2, animated=self.blit)
    self.speed_plt,  = self.ax_speed.plot(
          self.t, self.speed,
          linestyle='-', marker='o', markeredgecolor='g', markerfacecolor='None',
          markersize=2, animated=self.blit)

    self.artists = [self.torque_plt, self.speed_plt] + self.text.values()
    print(' done!')

  def init_plot (self):
    sys.stdout.write('Fetching 1st data batch ...')
    sys.stdout.flush()

    self.fetch_data()
    print(' done!')

    self.t0 = self.t[-1]

    sys.stdout.write('Decorating plots ...')
    sys.stdout.flush()

    self.ax_torque.set_ylabel('Torque [Nm]')
    self.ax_torque.grid(True, which='both')

    self.ax_speed.set_xlabel('Time [s] (bottom:data, top:plot)')
    self.ax_speed.set_ylabel('Ang. speed [Hz]')
    self.ax_speed.grid(True, which='both')

    self.ax_text.set_axis_off()
    self.ax_text.set_frame_on(False)
    print(' done!')

    return self.artists

  def fetch_data(self):
    # Get data
    i = 0
    data_dt = 0
    while i < self.ndata:
        [key, string] = self.socket.recv_multipart()
        datstr        = string.split()
        dtime         = float(datstr[0])
        if dtime > self.t[-1] or np.isnan(self.t[-1]):
          self.t.append(dtime)

        if key == u'20':
          self.torque.append(float(datstr[1]))
        elif key == u'1':
          self.speed.append(float(datstr[1]))

        if i == 0:
            data_dt = self.t[-1]

        i += 1

    data_dt = self.t[-1] - data_dt
    return data_dt

  def update_plot(self):
    # Update lines
    t = map (lambda x: x - self.t0, self.t)
    self.torque_plt.set_data(t, self.torque)
    self.speed_plt.set_data(self.t, self.speed)

    # Update axes
    # recompute the ax.dataLim
    self.ax_torque.relim()
    self.ax_speed.relim()
    # update ax.viewLim using the new dataLim
    self.ax_torque.autoscale_view(scalex=False)
    self.ax_torque.autoscale_view(tight=True, scaley=False)
    self.ax_speed.autoscale_view(scalex=False)
    self.ax_speed.autoscale_view(tight=True, scaley=False)

  def update (self, frame):
    # Update data
    fetch_time = time()
    data_dt = self.fetch_data()
    fetch_time = time() - fetch_time
    #print('fetching time: {}'.format(fetch_time))

    # Update graphs
    self.update_plot()

    # Update text
    self.text['time'].set_text(self.text_tmpl['time'].format(time() - self.start_t))
    self.text['lastup'].set_text(self.text_tmpl['lastup'].format(self.t[-1]))
    self.text['ndata'].set_text(self.text_tmpl['ndata'].format(self.ndata))

    # Update amount of data requested to logger to match realtime
    dt          = time() - self.lstframe - fetch_time
    try:
      self.ndata *= dt / data_dt
    except ZeroDivisionError:
      pass

    self.text['fps'].set_text(self.text_tmpl['fps'].format(1.0/dt))

    self.lstframe = time()

    return self.artists

  def run (self):
    self.ani  = FuncAnimation(self.fig, self.update, init_func=self.init_plot,
                              frames=None, interval=100,
                              blit=self.blit)
    plt.show()
    return

if __name__ == "__main__":
  import getopt

  def parse_options (args):
    hlp_str = 'test.py -d <deque size>'
    options = {'DSize':1000}
    try:
      opts, args = getopt.getopt(args,"hd:",["dequesize="])
    except getopt.GetoptError:
      print (hlp_str)
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
        print (hlp_str)
        sys.exit()
      elif opt in ("-d", "--dequesize"):
        options['DSize'] = int(arg)

    return options

  opts = parse_options(sys.argv[1:])
  pp = plotter(**opts)
  pp.run()

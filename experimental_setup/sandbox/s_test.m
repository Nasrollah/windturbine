data = load("friction_experiment_W.dat");
W = data(:,2);
t = data(:,1);

# At t == 68 the motor is already off
tf = t>68;

# Extract values of Omega and t when the motor is off
W_off = W(tf);
t_off = t(tf);

# Omega has NaN values, remove them!
idx   = find (!isnan(W_off));
t_off = t_off(idx);
W_off = W_off(idx);

n = length (W_off); % length of data

# calcjulate time derivative from a smoothed signal
# This part changes when using the signal package.
# Check butter, fitlfilt and sgolayfilt.

%Ws           = postpad (spencer (W_off), n);
%idx          = find (Ws!=0);
%Ws           = Ws(idx);
%ts           = t_off(idx);

pkg load signal
ln            = 21;
Ws            = sgolayfilt (W_off, 1, ln);
idx           = (2*ln):(n-2*ln+1);
Ws            = Ws(idx);
ts            = t_off(idx);

# Take derivative using diff
dWsdt        = diff (Ws) ./ diff (ts);
dWsdt(end+1) = dWsdt(end);

figure(1)
subplot(2,1,1)
plot(t_off,W_off,'or;Omega;',ts,Ws,'-b;Smoothed;');
axis tight
ylabel('signal')

subplot(2,1,2)
plot(ts,dWsdt,'or;dW/dt smooth;');
axis tight
xlabel('time')
ylabel('signal')

[alpha, S] = polyfit(Ws, dWsdt, 2);
alpha_err  = sqrt (diag (S.C) / S.df) * S.normr;

figure(2);
plot (Ws, dWsdt, 'og', Ws, polyval(alpha,Ws),'-k');
axis tight
xlabel('Omega')
ylabel('Omega dot')



## Write ODE
1;%

function dwdt = turbine (t, w, pp, tau_M, I)
  tau_F = polyval (pp,w); 
  dwdt  = ( tau_M (t) + tau_F) / I;
endfunction

alpha = [-1.9e-4 -2.5e-2 0] / 2 / pi;          # Friction force function
tauM  = @(t) 1e-1*ones (size (t));             # Motor torque
I     = 0.30;                                  # Moment of inertia

t  = linspace (0, 600, 100).'; # time vector
w0 = 0; %2 * pi * 2.83;

[t w] = ode45 (@(t,x)turbine(t,x,alpha,tauM,I), t, w0);
freq  = w / 2 / pi;

figure(1)
clf;
plot (t, freq, '-r');
xlabel ('Time [s]');
ylabel ('Rotation frequency [Hz]');
axis tight
grid on

## Wont work unless run s_test first
#hold on
#plot(t_off-t_off(1),W_off,'o');
#test
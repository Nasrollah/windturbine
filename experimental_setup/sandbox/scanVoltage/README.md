﻿Experiment Scanning Voltage

Measured Veriables:

w [rad/s]
I [A]

Start conditions:

U = 1-12 V (1 Volt step)

Air pressure 		971.1 hPa
Air relativ humidity 	46 %
Air temperatur		18 °C
Air speed		0 m/s


Description:

The purpose of this experiment is to measure omega and the current while scanning voltage. The torque and the power
can be calculated (electical and mechanical). The measurment starts by 1 Volt and ends by 12 Volts. The windturbine
had in the first measurement 1 min time to get stable and in the second experiment 1.5 min to look if the windturbine
was surly stable in the fist measurement. The current and the voltage were written down in the Excel-file "current.xlsx" during the experiment.

This measurement was repeated twice.

Errors:
The estimated rate started by 0.2 Hz and went up with the speed to 7.4 Hz.
The speedsensor has a error it self.
The current fluctuated during the experiment. The lower the voltage was set the more fluctuated the current.

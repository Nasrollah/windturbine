# Experiment Mechanical Friction

## Description

This measurement has the purpose to describe the behavior of the mechnical friction of the windtubine (including air friction).

The windturbine is pushed by the motor (at given voltage and current) untilt a steady state is reached.
After letting the windturbine stabilize, the electricity was switched off and the motor disconnected from th epower supply (open circuit).
At this point there is no electromagnetic friction, only mechanical friction.

The windturbine slows down until it stops.

The angular speed was measured during this process.

This measurement was repeated five times.

## Measured Veriables

Rtational speed of the turbine: w [rad/s]

## Experiments

### First experiment
Data stored in `fricton1_*.dat`

### Starting conditions:

 - Power supply voltage:        12 V 
 - Power suply current:         0.120 A
 - Power                        1.44 W
 - Air pressure                 970.6 hPa
 - Air relativ humidity         48 %
 - Air temperatur               17.7 °C
 - Air speed                    0 m/s
 - Stable rotation speeed       7.2 Hz (encoder)

### Notes
- The estimated rate started at 7.2 Hz and went down with the speed to 0.2 Hz.
- The speedsensor has a error itself.
- The current fluctuated in the beginning between 0.110-0.130 A.

### Second experiment
Data stored in `fricton2_*.dat`

### Starting conditions:

 - Power supply voltage:        35 V 
 - Power suply current:         0.255 A
 - Power                        8.925 W
 - Air pressure                 971.0 hPa
 - Air relativ humidity         47 %
 - Air temperatur               16.3 °C
 - Air speed                    0 m/s
 - Stable rotation speeed       19 Hz (encoder)

### Notes
- The estimated rate started at 19 Hz and went down with the speed to 0.2 Hz.
- The speedsensor has a error itself.
- The current fluctuated in the beginning between 0.251-0.264 A.
fname =@(i)sprintf ("friction2_%d.dat", i);

#for i=1:5
#  tmp = load (fname(i));
#  plot(tmp(:,1), tmp(:,2),sprintf('s;%d;',i));
#  axis tight
#  pause
#endfor

pkg load signal
# friction1
#t0 = [25, 8, 13, 15, 8];
# friction2
t0 = [12, 13, 7.3, 8, 12];

for i=1:5
  tmp = load (fname(i));
  tf = (tmp(:,1) >= t0(i)) & ~isnan (tmp(:,2));% & tmp(:,2) >= 0.8;
  t{i} = tmp(tf,1); t{i} = t{i} - t{i}(1);
  w{i} = tmp(tf,2);

  dt = diff (t{i});
  dwn{i} = diff (w{i}) ./ dt;
  dwn{i}(end+1) = dwn{i}(end);

  nb = 2;
  breaks = linspace(t{i}(2), t{i}(end-1), nb) + 0.5 * min(dt) * randn(1,nb);
  pp{i} = splinefit (t{i}, w{i}, breaks);
  dpp{i} = ppder (pp{i},1);

  wf{i} = sgolayfilt (w{i}, 1, 9);
  dwf{i} = diff (wf{i}) ./ diff (t{i});
  dwf{i}(end+1) = dwf{i}(end);

  aw(i,:) = polyfit (ppval(pp{i},t{i}), ppval(dpp{i},t{i}), 1);
endfor

figure (1)
clf
hold on
for i=1:5
  h = plot(t{i}, ppval(pp{i}, t{i}), t{i}, w{i},'s');
  set(h(2), 'color', get(h(1), 'color'));
endfor
axis tight
ylabel ("Angular speed [Hz]")
xlabel ("Time [s]")
hold off

figure (2)
clf
hold on
for i=1:5
#  plot(ppval(pp{i},t{i}), ppval(dpp{i},t{i}));
#  plot(ppval(pp{i},t{i}), ppval(dpp{i},t{i}), '-', w{i}, dwn{i}, 's');
#  plot(ppval(pp{i},t{i}), ppval(dpp{i},t{i}), '-', wf{i}, dwf{i}, 's');
  w_ = ppval(pp{i},t{i});
  h = plot(w_, ppval(dpp{i},t{i}), 'o', w_, polyval(aw(i,:), w_), '-');
  set(h(2), 'color', get(h(1), 'color'));

endfor
axis tight
ylabel ("Angular acceleration [Hz/s]")
xlabel ("Angular speed [Hz]")
hold off

tt = unique (vertcat (t{:}));
clear w_ a_
for i=1:5;
  w_{i} = ppval(pp{i},tt);
  a_{i} = ppval(dpp{i},tt);
endfor
w_ = vertcat (w_{:});
a_ = vertcat (a_{:});
awm = splinefit(w_,a_,[min(w_) 1 3 max(w_)], 'order', 1);
T = max(cellfun(@max,t));
Fr = @(t,x) ppval(awm,x);
for i=1:5;
 [tsim{i} wsim{i}] = ode45 (Fr, [0 T], w{i}(1));
endfor

#awm = mean (aw);
#T = max(cellfun(@max,t));
#Fr = @(t,x) polyval(awm,x);
#for i=1:5;
# [tsim{i} wsim{i}] = ode45 (Fr, [0 T], w{i}(1));
#endfor

figure (3)
clf
hold on
for i=1:5
  h = plot(tsim{i}, wsim{i}, t{i}, w{i},'s');
  set(h(2), 'color', get(h(1), 'color'));
endfor
axis tight
ylabel ("Angular speed [Hz]")
xlabel ("Time [s]")
hold off

res = cellfun(@(x,y,xs,ys) interp1(xs,ys,x) - y, t,w,tsim,wsim,'unif', 0);

figure(4)
clf
hold on
for i=1:5
  plot(w{i},res{i},'s');
endfor
axis tight
ylabel('Residual (meas - sim)')
xlabel('measured angular speed');

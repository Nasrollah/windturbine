%% Copyright (C) 2016 - 2017 Matthias Pasquon
%% Copyright (C) 2017 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Matthias Pasquon <matthias.pasquon@hsr.ch> (26.06.2016)
%% Adapted by: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [CFT fctor varargout] = ...
              dmsm_iteration (lambda, v_ref, theta_interval, rtheta, scaleG, ...
                                     vt, gamma, c_over_eta, iter_params)
% DMSM_ITERATION
% Function to calculate the iteration for the induction factor fctor and the
% corresponding aerodynamical forces on the blade in the streamtube
%
%     [FT fctor] = DMSM_ITERATION (lambda, scaleG, delta, ...
%                                   c_over_eta, vt, v_ref, rtheta, ...
%                                   theta_interval, iter_params)
%     [FT fctor info] = DMSM_ITERATION (...)
%
%     See also dmsm

  % interference factor
  fctor = iter_params.u_factor;
  tol   = iter_params.AbsTol;

  CDCL_func = iter_params.coeff_interp_func;

  % Integral of 1
  Intone = theta_interval(2) - theta_interval(1);
  % Integral of -sin (theta)
  Intsin = cos (theta_interval(2)) - cos (theta_interval(1));
  % Integral of tan (theta)
  f      =@(x)log (abs (sec (x)));
  Inttan = f (theta_interval(2)) - f (theta_interval(1));
  % Integral of - sin (theta) * tan (theta)
  f      =@(x)sin (x) + log (cos (x/2) - sin (x/2)) ...
                      - log (sin (x/2) + cos (x/2));
  IntST  = f (theta_interval(2)) - f (theta_interval(1));

  % Iteration
  while true

    v_blade = fctor * v_ref;    % Reduced wind velocity on blade (eq. 3.9)
    TSR     = vt / v_blade;     % Local Tip speed ratio (eq. 2.14)

    % Relative velocity on blade (vr): (eq. 2.1)
    X2p1    = TSR.^2  + 1;
    Xx2     = 2 * TSR;
    tmp     = sqrt (X2p1 - Xx2 * rtheta(2));
    v_relbl = abs (v_blade) * tmp;

    % Angle of attack (eq. 2.3 & 4.5)
    if lambda >= 1
       phi = asin (rtheta(1) / tmp);
    else
       phi = sign (rtheta(1)) * acos ( (vt - v_blade * rtheta(2)) / v_relbl );
    end
    alpha = phi + gamma; % angle of attack + pitch

    %inteprolation of CL and CD
    % Local blade Reynolds number (eq. 2.2)
    Re          = v_relbl * c_over_eta;
    [CL CD Rex] = CDCL_func (alpha, Re);
    % CN = normal coefficient & CT = tangential coefficient (eq. 2.4 & 2.5)
    car = cos (alpha);
    sar = sin (alpha);
    CN  = CL * car + CD * sar;
    CT  = CL * sar - CD * car;

    % Function to find interference factor u (eq. 3.13)
    %TODO consider the special case when the stream tube includes the limits.
    #GN   = CN * ( X2p1 * Intone + Xx2 * Intsin );
    #GT   = CT * ( X2p1 * Inttan + Xx2 * IntST  );
    #G    = scaleG * (GN - GT);
    G     = scaleG * tmp * (CN - CT * rtheta(2)/rtheta(1));

    % interference factor for next iteration process (eq. 3.14)
    #fctor_next = pi / ( G + pi );
    fctor_next = 1 / ( G + 1 );
  
    if (fctor - fctor_next < tol)
      break
    else
      fctor = fctor_next;
    end%if
  end%while
  
  % Normal & Tangential force (eq. 2.6-2.7)
  % TODO: use v_inf (away from turbine) intead of v_inf of the streamtube
  vrel2 = v_relbl^2;
  CFN   = CN * vrel2;
  CFT   = CT * vrel2;

  if nargout > 1
    % UP: Reduced wind velocity in middle of turbine (eq. 3.10)
    % DOWN: Reduced wind velocity after turbine (eq. 3.12)
    v_red = (2 * fctor - 1) * v_ref;
    varargout{1} = struct (
    'alpha', alpha, ...
    'v_reduc', v_red, ...
    'v_blade', v_blade, ...
    'v_relbl', v_relbl, ...
    'TSR', TSR, ...
    'Re', Re, ...
    'Re_interp', Rex, ...   %Interpolated Reynolds number
    'CL', CL, ...
    'CD', CD, ...
    'CN', CN, ...
    'CT', CT, ...
    'CFN', CFN
    );
  end%if
end

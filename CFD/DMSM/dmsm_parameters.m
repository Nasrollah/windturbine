%% Copyright (C) 2016 - 2017 Matthias Pasquon
%% Copyright (C) 2017 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Matthias Pasquon <matthias.pasquon@hsr.ch> (26.06.2016)
%% Adapted by: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function param = dmsm_parameters()
% DMSM_PARAMETERS
% Function to define all input parameters for the calculations
%
%    P = DMSM_PARAMETERS ()
%
%    See also dmsm

  % TODO use inputParser to update selected parameters

  % Blade parameter
  param.c       = 0.1;                              % Blade chord [m]
  param.NB      = 3;                                % Number of blades [-]
  param.gamma   = 0;                                % Blade pitch angle [rad]

  % Turbine parameters
  param.R  = 0.6;                                   % Turbine radius [m]
  param.hT = 0.8;                                  % Turbine height [m]
  param.SA = 2 * param.hT * param.R;                % Swept area [m^2]

  %-------------------------------------------------------------------------%
  % Free stream (air) ------------------------------------------------------%
  param.rho     = 1.225;                        % Density of fluid [kg/m^3]
  param.eta_air = 1.4607e-5;                    % Kin. viscosity at 15�C [m/^2s]
  param.v_inf   = 12;                           % Free streamvelocity [m/s]
  param.Re_inf  = 2 * param.rho * param.R * param.v_inf / param.eta_air;% Re [-]

  %-------------------------------------------------------------------------%
  % values needed for calculations -----------------------------------------%
  % TODO compute optimal number of tubes
  param.NS        = round (param.R * pi / (param.c*1.3)); % # of streamtubes
  dtheta          = (pi / param.NS) * ones (param.NS, 1);
  param.dtheta    = dtheta;                    % Streamtube angles [rad] 
  param.theta_bnd = bsxfun ( @minus, ...
     cumsum (dtheta), [dtheta(1) 0]) - pi/2;   % Streamtube boundaries [rad] 
  param.theta     = mean (param.theta_bnd, 2); % Streamtube centers [rad] 

  %-------------------------------------------------------------------------%
   % Parameters for the iteration algorithm
  param.iteration = struct ();

  param.iteration.u_factor = 1;    % Initial vlaue for velocity induction factor
  param.iteration.AbsTol   = 1e-8; % Stoping tolerance for iterations
  param.iteration.reset_u  = false;% reset u_factor for next iteration?
  param.iteration.int_eps  = 1e-5; % hack to avoid singularity in the integral for the interaction factor
  % Function to interpolate Lift and Drag coefficients
  % TODO interace with table generator, like xfoil
  NACA_profile                      = 'NACA0012';                 % NACA-profile
  NACA_file                         = [NACA_profile, '.xlsx'];
  param.iteration.coeff_interp_func = @(x,y)default_CDCL_interp_func(x,y,NACA_file);

end%function

%!demo
%! p = dmsm_parameters ();
%! xb = p.R * cos([p.theta_bnd(:,1); p.theta_bnd(end,2)]);
%! yb = p.R * sin([p.theta_bnd(:,1); p.theta_bnd(end,2)]);
%! xc = p.R * cos(p.theta);
%! yc = p.R * sin(p.theta);
%!
%! bladex = bladey = zeros (p.NS,2);
%! blade0 = [0 0; -p.c/2 p.c/2];
%! for i=1:p.NS
%!  bladex(i,:) = xc(i) + [cos(p.theta(i)) -sin(p.theta(i))] * blade0;
%!  bladey(i,:) = yc(i) + [sin(p.theta(i)) cos(p.theta(i))] * blade0;
%! endfor
%!
%! t = linspace (-0.5, 0.5, 100).' * pi;
%! c = p.R .* [cos(t) sin(t)];
%!
%! figure (1); clf;
%! h = plot(c(:,1),c(:,2),'k--', xb,yb,'og', xc, yc, 'xr'); 
%! set (h(2:3), 'linewidth', 2);
%! line (bladex.',bladey.', 'color', 'b', 'linewidth', 2);
%! axis image
%! v = axis() .* [1 1.05 1.05 1.05];
%! axis (v);
%! line (repmat(v(1:2).',1,p.NS+1), repmat(yb.',2,1));
%! xlabel ('x')
%! ylabel ('y') 
%! box off



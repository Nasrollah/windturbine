%% Copyright (C) 2016 - 2017 Matthias Pasquon
%% Copyright (C) 2017 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Matthias Pasquon <matthias.pasquon@hsr.ch> (26.06.2016)
%% Adapted by: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [Torque, Cp, varargout] = dmsm (lambda, param)
% DMSM  Implementation of the double multiple streamtube model for vertial axis
% wind turbines
%    R = DMSM (P) calculates a struct of results using paramters P 
%
%    See also dmsm_parameters, dmsm_liftdrag_coeffs, dmsm_iteration

  % Set output flags for compuation of iteration information
  return_info = false;
  if nargout > 3
    retrun_info = true;
  end%if

  % Turbine rotational velocity (eq. 2.14)
  omega            = param.v_inf * lambda / param.R;
  v_tang           = param.R * omega;         % Tangetial velocity
  c_over_eta       = param.c / param.eta_air; % for Reynolds number calculation
  gamma            = param.gamma;             % Pitch of blade
  scaleG           = (param.NB * param.c / ( 8 * pi * param.R));
  scaleForce       = 0.5 * param.rho * param.c * param.hT;

  % radius unitary vector pointing to center of streamtube
  theta  = param.theta;
  rtheta = [cos(theta) sin(theta)];
  
  % intervals of integration for each streamtube
  theta_int       = param.theta_bnd;
  theta_int(1)   += param.iteration.int_eps;
  theta_int(end) -= param.iteration.int_eps;

  %interference factors
  u_factorUP = u_factorDN = ones (param.NS,1);

  % Inputs for iteration solver.
  % Empties are placeholders
  args = {[], [], [], []...
          v_tang, gamma, c_over_eta, param.iteration};

  argsUP    = args;
  argsUP{1} = param.v_inf;
  argsUP{4} = scaleG;

  argsDN    = args;
  argsDN{4} = -scaleG;     % sign given by sign (cos (theta))
  
  % Pre-allocation of result arrays
  FtUP = u_factorUP = zeros (param.NS, 1);
  FtDN = u_factorDN = zeros (param.NS, 1);
  % TODO allocate iter* when return_info

  % Loop over all streamtubes
  opp_i = param.NS:-1:1;
  for i = 1:param.NS
    argsUP{2} = theta_int(i,:);
    argsUP{3} = rtheta(i,:);

    %---------------------------------------------------------------------%
    % UPSTREAM CALCULATION -----------------------------------------------%
    if ~argsUP{end}.reset_u
      % initialize interference factor with previous
      if i > 1
        argsUP{end}.u_factor = u_factorUP(i-1);
      end%if
    end%if

    % Iteration solver for upstream iteration
    if return_info
      [FtUP(i), u_factorUP(i), iterUP(i)] = dmsm_iteration (lambda, argsUP{:});
    else
      [FtUP(i), u_factorUP(i)]            = dmsm_iteration (lambda, argsUP{:});
    end%if

    %---------------------------------------------------------------------%
    % DOWNSTREAM CALCULATION ---------------------------------------------%
    % Reduced wind velocity in middle of turbine (eq. 3.10)
    argsDN{1} = param.v_inf * (2 * u_factorUP(i) - 1); 

    j         = opp_i(i); % take angle at other extrema
    argsDN{2} = theta_int(j,:) + pi;
    argsDN{3} = -rtheta(j,:);

    if ~argsDN{end}.reset_u
      % From Paraschivoiu:
      % "The iterative process is initialized by u_facotrDN(j) = u_factorUP(i)"
      argsDN{end}.u_factor = u_factorUP(i);
    end%if

    % Iteration solver for downstream iteration
    if return_info
      [FtDN(i), u_factorDN(i), iterDN(i)] = dmsm_iteration (lambda, argsDN{:});
    else
      [FtDN(i), u_factorDN(i)]            = dmsm_iteration (lambda, argsDN{:});
    end%if
    
  end%for over streamtubes

  %---------------------------------------------------------------------%
  % TOTAL TURBINE CALCULATION ------------------------------------------%
  % Total average torque
  x = [theta; theta+pi];
  y = [FtUP; flipud(FtDN)];
  Torque = param.NB * param.R * ( scaleForce * trapz (x, y)  / (2 * pi) );

  % Total power coefficient (eq. 2.10, 2.13 for up- and downstream)
  Cp = lambda * Torque / ...
                     (0.5 * param.rho * param.R * param.v_inf^2 * param.SA );

  if nargout == 3
    % return iteration information
    varargout{1} = scaleForce * [FtDN FtUP];
  end%if

  if return_info
    % return iteration information
    varargout{end} = struct ('up', iterUP, 'down', iterDN);
  end%if

end%function

%!demo
%! warning ('This demo takes about 1 minute to run')
%! p  = dmsm_parameters ();
%! nl = 30;
%! l  = linspace (0.25, 10, nl);
%!
%! % With u restart
%! p.iteration.reset_u = true;
%! T  = Cp = zeros (nl, 1);
%! tic
%! for i = 1:nl
%!   [T(i) Cp(i) F] = dmsm (l(i), p);
%! end%for
%! toc
%! Ft = [F(:,1); flipud(F(:,2))];
%!
%! % Without u restart
%! p.iteration.reset_u = false;
%! Tr  = Cpr = zeros (nl, 1);
%! tic
%! for i = 1:nl
%!   [Tr(i) Cpr(i) F] = dmsm (l(i), p);
%! end%for
%! toc
%! Ftr = [F(:,1); flipud(F(:,2))];
%!
%! figure (1)
%! subplot (2,1,1);
%! h = plot (l,[T Tr], 'o-');
%! for i=1:2; set (h(i), 'markerfacecolor', get (h(i), 'color')); end
%! axis tight;
%! ylabel ('Torque [Nm]');
%! grid on
%! legend (h(1), {'reseting u'})
%! subplot (2,1,2);
%! h = plot (l,[Cp Cpr], 'o-');
%! for i=1:2; set (h(i), 'markerfacecolor', get (h(i), 'color')); end
%! axis tight;
%! ylabel ('Cp [-]');
%! grid on
%! legend (h(1), {'reseting u'})
%! xlabel ('Tip/Wind speed ratio')
%!
%! figure (2)
%! theta = [p.theta; p.theta+pi];
%! h = plot (theta * 180 / pi,[Ft Ftr], 'o-');
%! for i=1:2; set (h(i), 'markerfacecolor', get (h(i), 'color')); end
%! axis tight;
%! ylabel ('Tangential Force [N]');
%! grid on
%! legend (h(1), {'reseting u'})
%! xlabel ('Angle [deg]')

%!demo
%! warning ('This demo takes about 1 minute to run')
%! p  = dmsm_parameters ();
%! l  = 6.5;
%! % reduce integration interval of divergent integral by this amount
%! n       = 10;
%! int_eps = logspace(-10,-1, n);
%!
%! % With u restart
%! p.iteration.reset_u = true;
%! T  = Cp = zeros (n, 1);
%! tic
%! for i = 1:n
%!   p.iteration.int_eps = int_eps(i);
%!   [T(i) Cp(i)] = dmsm (l, p);
%! end%for
%! toc
%!
%! % Without u restart
%! p.iteration.reset_u = false;
%! Tr  = Cpr = zeros (n, 1);
%! tic
%! for i = 1:n
%!   p.iteration.int_eps = int_eps(i);
%!   [Tr(i) Cpr(i)] = dmsm (l, p);
%! end%for
%! toc
%! 
%! figure (1)
%! subplot (2,1,1);
%! h = semilogx (int_eps,[T Tr], 'o');
%! for i=1:2; set (h(i), 'markerfacecolor', get (h(i), 'color')); end
%! axis tight
%! ylabel ('Torque [Nm]');
%! grid on
%! legend (h(1), {'reseting u'})
%! subplot (2,1,2);
%! h = semilogx (int_eps,[Cp Cpr], 'o');
%! for i=1:2; set (h(i), 'markerfacecolor', get (h(i), 'color')); end
%! axis tight
%! ylabel ('Cp [-]');
%! grid on
%! legend (h(1), {'reseting u'})
%! xlabel ('interval eps')

%!test
%! p = dmsm_parameters();
%! tic
%! [t c] = dmsm(0.45, p);
%! toc
%! printf ('Torque: %g\n', t);
%! printf ('Cp    : %g\n', c);


%!test
%! p = dmsm_parameters();
%! [t c] = dmsm(0.45, p);
%! printf ('Torque: %g\n', t);
%! printf ('Cp    : %g\n', c);

%% Copyright (C) 2016 - 2017 Matthias Pasquon
%% Copyright (C) 2017 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Matthias Pasquon <matthias.pasquon@hsr.ch> (26.06.2016)
%% Adapted by: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [Re aCL aCD CL CD] = dmsm_liftdrag_coeffs (NACA_file)
% DMSM_LIFTDRAG_COEFFS
% Function to read lift and drag coefficients from external excel data file
%
%    [R AL AD L D] = DMSM_LIFTDRAG_COEFFS (FILE)
%
%    See also default_CDCL_interp_func

  % Check if data is already available
  if ~exist (NACA_file,'file');
    error ('dmsm:Invalid-input-arg', ...
           'dmsm_liftdrag_coeffs: Excel File is not available');
  end%if

  CL      = xlsread (NACA_file,'CL');
  CD      = xlsread (NACA_file,'CD');

  Re  = CL(1,2:end);
  aCL = CL(2:end,1) * pi / 180; % Tabulated values are in degrees
  aCD = CD(2:end,1) * pi / 180; % Tabulated values are in degrees
  CL  = CL(2:end,2:end);
  CD  = CD(2:end,2:end);

end%function

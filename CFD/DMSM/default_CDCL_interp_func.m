%% Copyright (C) 2017 Juan Pablo Carbajal
%%
%% This program is free software; you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation; either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
%% Based on the orginal code used in DMSM iteration algorithms

function [CL CD Re_interp] = default_CDCL_interp_func (alpha, Re, file)
% DEFAULT_CDCL_INTERP_FUNC interpolates lift and drag coefficnt using tabulated
% values
%    Nearest neighbour interpolation in Reynolds numer and linear interpolation
%    in angle of attack.
%
%    [L D Rx] = DEFAULT_CDCL_INTERP_FUNC (A, R, FILE)
%    A: angle of attack (radians)
%    R: Reynolds number
%    FILE: NACA filename (Excel) with tabulated values
%    L: Lift coefficient (interpolated)
%    D: Drag coefficent (interpolated)
%    Rx: Nearest tabulated value of R
%
%    See also dmsm_liftdrag_coeffs

  persistent Re_tab CLpp CDpp
  if isempty (Re_tab)
    disp ('default_CDCL_interp_func: Loading tables ...')
    %% Lift and drag coefficients
    [Re_tab, aCL_tab, aCD_tab, CL_tab, CD_tab] = dmsm_liftdrag_coeffs (file);

    nRe  = length (Re_tab);
    CLpp = CDpp = cell (nRe,1);
    for i=1:nRe
      CLpp{i} = interp1 (aCL_tab, CL_tab(:, i), "linear", "pp");
      CDpp{i} = interp1 (aCD_tab, CD_tab(:, i), "linear", "pp");
    endfor
    disp('done!')

  end%if

  % Searching for index of closest Re value in CL/CD List
  [~, idx_Re] = min ( abs ( Re_tab - Re));
  Re_interp   = Re_tab(idx_Re); %closest value for Re

  abs_a = abs (alpha);
  sig_a = sign (alpha);
  % Interpolation of CL (when negative alpha also negative CL!)
  %CL = sig_a * interp1 (aCL_tab, CL_tab(:,idx_Re), abs_a);
  CL = sig_a * ppval (CLpp{idx_Re}, abs_a);
  % Interpolation of CD
  %CD = interp1 (aCD_tab, CD_tab(:,idx_Re), abs_a);
  CD = ppval (CDpp{idx_Re}, abs_a);
end%function
